package com.kamilstosik.stopwatch

import android.os.Handler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Matchers
import org.mockito.Matchers.any
import org.mockito.Mockito.*

class StopwatchUnitTest {

    val clockMock = mock(ClockWrapper::class.java)
    val presenter = mock(MainContract.MainPresenter::class.java)
    val stopwatch = StopwatchImpl(presenter)
    val handler = mock(Handler::class.java)

    @Before
    fun setup() {
        stopwatch.clock = clockMock
        stopwatch.handler = handler
    }

    @Test
    fun startTest() {
        `when`(stopwatch.clock.getElapsedRealTime()).thenReturn(1000L)
        `when`(handler.post(any(Runnable::class.java))).thenAnswer {}
        stopwatch.start()
        Assert.assertTrue(stopwatch.isRunning)
        Assert.assertTrue(stopwatch.startTime > 0)
    }


    @Test
    fun pauseTest() {
        `when`(stopwatch.clock.getElapsedRealTime()).thenReturn(1000L)
        stopwatch.startTime = 0L
        stopwatch.isRunning = true
        stopwatch.pause()
        Assert.assertTrue(!stopwatch.isRunning)
        Assert.assertTrue(stopwatch.saveTime > 0L)
    }

    @Test
    fun stopTest() {
        stopwatch.stop()
        Assert.assertTrue(!stopwatch.isRunning)
        Assert.assertTrue(stopwatch.saveTime == 0L)
        Assert.assertTrue(stopwatch.startTime == 0L)
    }

    @Test
    fun tickTest() {
        stopwatch.tick()
        verify(handler, atLeastOnce()).postDelayed(any(Runnable::class.java), Matchers.anyLong())
    }

    @Test
    fun onTick() {
        stopwatch.onTick()
        verify(presenter, atLeastOnce()).onTimeUpdate(Matchers.anyLong())
        verify(handler, atLeastOnce()).postDelayed(any(Runnable::class.java), Matchers.anyLong())
    }
}
