package com.kamilstosik.stopwatch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainContract.MainView {

    private val presenter = MainPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        start.setOnClickListener { presenter.start() }
        stop.setOnClickListener { presenter.stop() }
        pause.setOnClickListener { presenter.pause() }
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        presenter.onSaveInstanceActivity(savedInstanceState)
        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        presenter.onRestoreInstanceActivity(savedInstanceState)
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        presenter.onResumeActivity()
    }

    override fun onPause() {
        presenter.onPauseActivity()
        super.onPause()
    }

    override fun updateTime(time: String) {
        display.text = time
    }

    override fun resetDisplay() {
        display.text = "00:00:00"
    }
}
