package com.kamilstosik.stopwatch

interface Stopwatch {
    fun start()
    fun pause()
    fun tick()
    fun stop()
    fun onTick()
}