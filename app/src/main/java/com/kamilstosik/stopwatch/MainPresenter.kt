package com.kamilstosik.stopwatch

import android.os.Bundle

class MainPresenter(private val view : MainContract.MainView) : MainContract.MainPresenter {

    private val stopwatch = StopwatchImpl(this)
    private val converter = TimeConverter()

    override fun onRestoreInstanceActivity(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            stopwatch.isRunning = it["isRunning"] as Boolean
            stopwatch.saveTime = it["saveTime"] as Long
        }
    }

    override fun onSaveInstanceActivity(savedInstanceState: Bundle) {
        savedInstanceState.putBoolean("isRunning", stopwatch.isRunning)
        savedInstanceState.putLong("saveTime", stopwatch.saveTime)
    }

    override fun onPauseActivity() {
        if(stopwatch.isRunning) {
            stopwatch.pause()
        } else {
            stopwatch.stop()
        }
    }

    override fun onResumeActivity() {}

    override fun start() {
        stopwatch.start()
    }

    override fun pause() {
        stopwatch.pause()
    }

    override fun stop() {
        stopwatch.stop()
        view.resetDisplay()
    }

    override fun onTimeUpdate(time: Long) {
        view.updateTime(converter.millisecondsToString(time))
    }
}