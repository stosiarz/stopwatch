package com.kamilstosik.stopwatch

import android.os.Bundle

class MainContract {

    interface MainView {
        fun updateTime(time : String)
        fun resetDisplay()
    }

    interface MainPresenter {
        fun onResumeActivity()
        fun onPauseActivity()
        fun onSaveInstanceActivity(savedInstanceState: Bundle)
        fun onRestoreInstanceActivity(savedInstanceState: Bundle?)
        fun start()
        fun pause()
        fun stop()
        fun onTimeUpdate(time : Long)
    }
}