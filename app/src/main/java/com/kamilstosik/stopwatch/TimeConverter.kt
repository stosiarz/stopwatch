package com.kamilstosik.stopwatch
import java.lang.StringBuilder

class TimeConverter {

    private val builder = StringBuilder()

    fun millisecondsToString(milliseconds: Long): String {
        builder.clear()
        val seconds = (milliseconds / 1000).toInt() % 60
        val minutes = (milliseconds / (1000 * 60) % 60).toInt()
        val hours = (milliseconds / (1000 * 60 * 60) % 24).toInt()
        return buildTimeString(seconds, minutes, hours, milliseconds)
    }

    private fun buildTimeString(seconds : Int, minutes : Int, hours : Int, milliseconds : Long) : String {
        return builder.append(valueToStr(hours))
            .append(":")
            .append(valueToStr(minutes))
            .append(":")
            .append(valueToStr(seconds))
            .append(":")
            .append(millisecondsToStr(milliseconds % 1000))
            .toString()
    }

    private fun valueToStr(value : Int) : String {
        return if (value < 10) "0$value" else value.toString()
    }

    private fun millisecondsToStr(milliseconds: Long) : String {
        return when {
            milliseconds<10 -> "00$milliseconds"
            milliseconds<100 -> "0$milliseconds"
            else -> milliseconds.toString()
        }
    }
}