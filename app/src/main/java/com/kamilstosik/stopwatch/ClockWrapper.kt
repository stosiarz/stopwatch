package com.kamilstosik.stopwatch

import android.os.SystemClock

open class ClockWrapper {

    open fun getElapsedRealTime() : Long {
        return SystemClock.elapsedRealtime()
    }
}