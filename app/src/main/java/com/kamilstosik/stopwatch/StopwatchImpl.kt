package com.kamilstosik.stopwatch

import android.os.Handler

open class StopwatchImpl(private val presenter: MainContract.MainPresenter) : Stopwatch {

    var clock = ClockWrapper()
    var handler = Handler()
    var startTime = 0L
    var isRunning = false
    var saveTime = 0L

    override fun start() {
        if(!isRunning) {
            isRunning = true
            startTime = clock.getElapsedRealTime() - saveTime
            tick()
        }
    }

    override fun tick() {
        handler.postDelayed(runTick, 10)
    }

    override fun pause() {
        if(isRunning) {
            isRunning = false
            saveTime = clock.getElapsedRealTime() - startTime
            handler.removeCallbacks(runTick)
        }
    }

    override fun stop() {
        isRunning = false
        startTime = 0L
        saveTime = 0L
        handler.removeCallbacks(runTick)
    }

    override fun onTick() {
        presenter.onTimeUpdate(clock.getElapsedRealTime() - startTime)
        tick()
    }

    private val runTick = Runnable {
        onTick()
    }
}